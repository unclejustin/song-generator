# Overview

## General Flow

1. Song structure
2. Key
3. Bpm
4. Chord progression for each part

## Song structures

AAA, ABAB, ABABCB, ABABCAB

## Chord Progressions

### Major

I ? ? IV/V

? = 1-6 randomly selected

### Minor

Random order of i VI VII

or

i ? ? VI/VII

? = 2-5 randomly selected

or

i ? ? V/V7

? = 2/3/4/6/7 randomly selected

## Chord progression simple rules

## Major simple chord progression rules

I ii iii IV V vi vii°
Start with I end with IV or V no vii°

## Minor simple chord progression rules

i ii° III iv v VI VII

Only use i VI and VII in any order

ex: i VII VI VII

Use the major chords to brighten up the song

i VI III VII = Sensitive Female Chord Progression

Make the v a dominant V or V7 to add tension and pull back to i

Andalusian cadence = i VII VI V
