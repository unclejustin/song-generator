import 'react-testing-library/cleanup-after-each'
import React from 'react'
import { render } from 'react-testing-library'
import utils from './utils'
import { StoreProvider } from './Store'
import App from './App'

describe('App', () => {
  it('Should call generateSong', () => {
    utils.generateSong = jest.fn(() => ({ structure: 'AAA', key: [], progressions: [] }))

    const { getByText } = render(
      <StoreProvider>
        <App />
      </StoreProvider>,
    )

    // const button = getByText("Generate Song");
    // button.click();
    expect(getByText('AAA', { exact: false })).toBeDefined()
  })
})
