import React from 'react'
import utils from './utils'
import { Store, actionTypes } from './Store'
import Structure from './components/Structure'
import Key from './components/Key'
import Bpm from './components/Bpm'
import Progressions from './components/Progressions'

import './App.css'

function App() {
  const { dispatch } = React.useContext(Store)
  const handleClick = () => {
    const song = utils.generateSong()
    dispatch({ type: actionTypes.SAVE_SONG, payload: song })
  }

  const [songLoaded, setSongLoaded] = React.useState(false)

  // Generate a song on load.
  React.useEffect(() => {
    if (songLoaded) return
    handleClick()
    setSongLoaded(true)
  })

  return (
    <section className='container'>
      <h1 className='appTitle'>Song Generator</h1>
      <section className='songContainer'>
        <div className='flexRow'>
          <Structure />
          <Bpm />
        </div>
        <hr className='separator' />
        <Key />
        <hr className='separator' />
        <Progressions />
        <button className='generateSongButton' onClick={handleClick}>
          Generate Song
        </button>
      </section>
    </section>
  )
}

export default App
