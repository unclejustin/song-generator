import { mockRandom, resetMockRandom } from 'jest-mock-random'
import utils from './utils'
jest.mock('lodash/shuffle', () => a => a)

describe('utils', () => {
  describe('getBPM', () => {
    it('Should return a number between 120 and 180', () => {
      mockRandom([0.5])
      expect(utils.getBPM()).toEqual(150)
      resetMockRandom()
    })
  })

  describe('getKey', () => {
    it('Should return the correct major key', () => {
      const aMajor = ['A', 'Bm', 'C#m', 'D', 'E', 'F#m', 'G#°']
      expect(utils.getKey('A')).toEqual(aMajor)

      const cMajor = ['C', 'Dm', 'Em', 'F', 'G', 'Am', 'B°']
      expect(utils.getKey('C')).toEqual(cMajor)
    })

    it('Should return the correct minor key', () => {
      const aMinor = ['Am', 'B°', 'C', 'Dm', 'E', 'F', 'G']
      expect(utils.getKey('A', false)).toEqual(aMinor)

      const cMinor = ['Cm', 'D°', 'D#', 'Fm', 'G', 'G#', 'A#']
      expect(utils.getKey('C', false)).toEqual(cMinor)
    })
  })

  describe('getKeyRoot', () => {
    it('Should return a key root', () => {
      mockRandom([0.0])
      expect(utils.getKeyRoot()).toEqual('A')
      resetMockRandom()
    })
  })

  describe('getMajorProgression', () => {
    it('Should return a major progression', () => {
      mockRandom([0.1, 0.4, 0.8])
      const key = utils.getKey('C')
      const progression = utils.getMajorProgression(key)
      expect(progression).toEqual(['C', 'Em', 'Am', 'F'])
      resetMockRandom()
    })
  })

  describe('getMinorProgression', () => {
    const key = utils.getKey('A', false)

    describe('first variation', () => {
      it('Should return some variation of i VI VII', () => {
        mockRandom([0.1])
        const progression = ['Am', 'F', 'G']
        expect(utils.getMinorProgression(key)).toEqual(progression)
        resetMockRandom()
      })
    })

    describe('second variation', () => {
      it('Should return i ? ? VI/VII', () => {
        mockRandom([0.4, 0.2, 0.4, 0.9])
        const progression = ['Am', 'C', 'Dm', 'G']
        expect(utils.getMinorProgression(key)).toEqual(progression)
        resetMockRandom()
      })
    })

    describe('third variation', () => {
      it('Should return i ? ? V/V7', () => {
        mockRandom([0.8, 0.6, 0.4, 0.9])
        const progression = ['Am', 'Dm', 'G', 'E7']
        expect(utils.getMinorProgression(key)).toEqual(progression)
        resetMockRandom()
      })
    })
  })

  describe('getStructure', () => {
    it('Should return some structure', () => {
      mockRandom([0.3])
      const structure = utils.getStructure()
      expect(structure).toEqual('ABAB')
      resetMockRandom()
    })
  })

  describe('generateSong', () => {
    it('Should generate a new random song', () => {
      mockRandom([0.5, 0.3, 0.6, 0.3, 0.1, 0.4, 0.8, 0.1, 0.4, 0.8])
      const song = utils.generateSong()
      const testSong = {
        bpm: 150,
        keyRoot: 'C',
        key: ['C', 'Dm', 'Em', 'F', 'G', 'Am', 'B°'],
        major: true,
        structure: 'ABAB',
        progressions: [['C', 'Em', 'Am', 'F'], ['C', 'Em', 'Am', 'F']],
      }

      expect(song).toEqual(testSong)
      resetMockRandom()
    })

    it('Should only update params not provided', () => {
      mockRandom([0.3])
      const testSong = {
        bpm: 180,
        keyRoot: 'C',
        key: ['C', 'Dm', 'Em', 'F', 'G', 'Am', 'B°'],
        major: true,
        structure: 'ABAB',
        progressions: [['C', 'Em', 'Am', 'F'], ['C', 'Em', 'Am', 'F']],
      }

      const song = utils.generateSong({ ...testSong, structure: undefined })

      expect(song).toEqual(testSong)
      resetMockRandom()
    })
  })
})
