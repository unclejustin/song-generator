import React, { createContext, useReducer } from 'react'
import PropTypes from 'prop-types'

export const Store = createContext()
const initialState = {
  song: {
    bpm: 0,
    key: [],
    keyRoot: '',
    major: true,
    structure: '',
    progressions: [],
  },
}

export const actionTypes = {
  SAVE_SONG: 'SAVE_SONG',
}

function reducer(state, { type, payload }) {
  switch (type) {
    case actionTypes.SAVE_SONG:
      return { ...state, song: payload }
    default:
      return state
  }
}

export function StoreProvider(props) {
  const [state, dispatch] = useReducer(reducer, initialState)
  const value = { state, dispatch }
  return <Store.Provider value={value}>{props.children}</Store.Provider>
}

StoreProvider.propTypes = { children: PropTypes.node.isRequired }
