import React, { useContext } from 'react'
import utils from '../utils'
import { Store, actionTypes } from '../Store'

export default function Bpm() {
  const { state, dispatch } = useContext(Store)
  const rand = () => {
    const bpm = utils.getBPM()
    dispatch({ type: actionTypes.SAVE_SONG, payload: { ...state.song, bpm } })
  }
  const {
    song: { bpm },
  } = state
  return (
    <div>
      <label className='label' onClick={rand}>
        Tempo
      </label>
      <p className='text'>{bpm}</p>
    </div>
  )
}
