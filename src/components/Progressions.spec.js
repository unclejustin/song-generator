import 'react-testing-library/cleanup-after-each'
import React from 'react'
import { render } from 'react-testing-library'
import utils from '../utils'
import { StoreProvider } from '../Store'
import Progressions from './Progressions'

describe('Progressions', () => {
  it('should call getProgressions when clicking the random button', () => {
    utils.getProgressions = jest.fn(() => [['A', 'B']])

    const { getByText } = render(
      <StoreProvider>
        <Progressions />
      </StoreProvider>,
    )

    getByText('Progressions').click()
    expect(getByText('A B', { exact: false })).toBeDefined()
  })
})
