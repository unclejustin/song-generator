import 'react-testing-library/cleanup-after-each'
import React from 'react'
import { render } from 'react-testing-library'
import utils from '../utils'
import { StoreProvider } from '../Store'
import Bpm from './Bpm'

describe('Bpm', () => {
  it('should call getBPM when clicking the random button', () => {
    utils.getBPM = jest.fn(() => 100)

    const { getByText } = render(
      <StoreProvider>
        <Bpm />
      </StoreProvider>,
    )

    getByText('Tempo').click()
    expect(getByText('100', { exact: false })).toBeDefined()
  })
})
