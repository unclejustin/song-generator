import React, { useContext } from 'react'
import utils from '../utils'
import { Store, actionTypes } from '../Store'

export default function Key() {
  const { state, dispatch } = useContext(Store)
  const {
    song: { key, major, structure, keyRoot },
  } = state
  const rand = () => {
    // When key updates generate new progressions
    const keyRoot = utils.getKeyRoot()
    const newKey = utils.getKey(keyRoot, major)
    const progressions = utils.getProgressions(structure, key, major)
    dispatch({
      type: actionTypes.SAVE_SONG,
      payload: { ...state.song, key: newKey, progressions },
    })
  }

  const toggle = () => {
    // When updating major also update key, and progressions
    const newMajor = !major
    const key = utils.getKey(keyRoot, newMajor)
    const progressions = utils.getProgressions(structure, key, newMajor)
    dispatch({
      type: actionTypes.SAVE_SONG,
      payload: { ...state.song, major: newMajor, key, progressions },
    })
  }
  return (
    <div style={{ position: 'relative' }}>
      <span className='label' onClick={() => rand()}>
        Key
      </span>
      <span className='label small' onClick={() => toggle()}>
        ({major ? 'major' : 'minor'})
      </span>
      <p className='text'>
        {keyRoot}
        {major ? '' : 'm'}
      </p>
      <p className='text small pullRight'>{key.join(' ')}</p>
    </div>
  )
}
