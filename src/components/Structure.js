import React, { useContext } from 'react'
import utils from '../utils'
import { Store, actionTypes } from '../Store'

export default function Structure() {
  const { state, dispatch } = useContext(Store)
  const {
    song: { structure, key, major },
  } = state

  const rand = () => {
    // When updating structure also update progressions
    const newStructure = utils.getStructure()
    const progressions = utils.getProgressions(newStructure, key, major)
    dispatch({
      type: actionTypes.SAVE_SONG,
      payload: { ...state.song, structure: newStructure, progressions },
    })
  }
  return (
    <div>
      <label className='label' onClick={() => rand()}>
        Structure
      </label>
      <p className='text'>{structure}</p>
    </div>
  )
}
