import React, { useContext } from 'react'
import utils from '../utils'
import { Store, actionTypes } from '../Store'

export default function Progressions() {
  const { state, dispatch } = useContext(Store)
  const {
    song: { progressions, structure, key, major },
  } = state
  const rand = () => {
    const newProgressions = utils.getProgressions(structure, key, major)
    dispatch({
      type: actionTypes.SAVE_SONG,
      payload: { ...state.song, progressions: newProgressions },
    })
  }

  const progression = (p, i) => <div key={i}>{p.join(' ')}</div>

  return (
    <div>
      <span className='label' onClick={() => rand()}>
        Progressions
      </span>
      <div className='text'>{progressions.map(progression)}</div>
    </div>
  )
}
