import 'react-testing-library/cleanup-after-each'
import React from 'react'
import { render } from 'react-testing-library'
import utils from '../utils'
import { StoreProvider } from '../Store'
import Key from './Key'

describe('Key', () => {
  it('should call getKeyRoot, getKey and getProgressions when clicking the random button', () => {
    utils.getKeyRoot = jest.fn(() => 'A')
    utils.getKey = jest.fn(() => ['A'])
    utils.getProgressions = jest.fn(() => [])

    const { getByText } = render(
      <StoreProvider>
        <Key />
      </StoreProvider>,
    )

    getByText('Key').click()
    expect(getByText('A', { exact: false })).toBeDefined()
  })

  it('should call getKey and getProgressions when clicking the random button', () => {
    utils.getKey = jest.fn(() => [])
    utils.getProgressions = jest.fn(() => [])

    const { getByText } = render(
      <StoreProvider>
        <Key />
      </StoreProvider>,
    )

    getByText('(major)').click()
    expect(getByText('(minor)')).toBeDefined()
  })
})
