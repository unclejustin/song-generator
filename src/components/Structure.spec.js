import 'react-testing-library/cleanup-after-each'
import React from 'react'
import { render } from 'react-testing-library'
import utils from '../utils'
import { StoreProvider } from '../Store'
import Structure from './Structure'

describe('Structure', () => {
  it('should call getStructure and getProgressions when clicking the random button', () => {
    utils.getStructure = jest.fn(() => 'AAA')
    utils.getProgressions = jest.fn(() => [])

    const { getByText } = render(
      <StoreProvider>
        <Structure />
      </StoreProvider>,
    )

    getByText('Structure').click()
    expect(getByText('AAA', { exact: false })).toBeDefined()
  })
})
