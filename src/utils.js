import shuffle from 'lodash/shuffle'
const notes = ['A', 'A#', 'B', 'C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#']

function _random(max, min = 0) {
  return Math.floor(Math.random() * (max - min + 1) + min)
}

function getBPM() {
  return _random(180, 120)
}

function getKey(note = 'A', major = true) {
  const majorIntervals = [0, 2, 4, 5, 7, 9, 11]
  const minorIntervals = [0, 2, 3, 5, 7, 8, 10]
  const majorVoices = ['', 'm', 'm', '', '', 'm', '°']
  const minorVoices = ['m', '°', '', 'm', '', '', '']

  const noteIndex = notes.indexOf(note)
  const tmpNotes = notes.slice(noteIndex).concat(notes.slice(0, noteIndex))
  const intervals = major ? majorIntervals : minorIntervals
  const voices = major ? majorVoices : minorVoices
  const chords = intervals.map((v, i) => tmpNotes[v] + voices[i])
  return chords
}

function getKeyRoot() {
  return notes[_random(notes.length)]
}

function getMajorProgression(key) {
  const fourFive = Math.random() < 0.5 ? 3 : 4
  return [key[0], key[_random(6)], key[_random(6)], key[fourFive]]
}

function getMinorProgression(key) {
  const randomProgression = _random(3, 1)

  if (randomProgression === 1) {
    // Random variation of i VI VII
    return shuffle([key[0], key[5], key[6]])
  }

  if (randomProgression === 2) {
    // i ? ? VI/VII
    return [key[0], key[_random(6, 1)], key[_random(6, 1)], key[_random(6, 5)]]
  }

  if (randomProgression === 3) {
    // i ? ? V/V7
    const seventh = Math.random() > 0.5 ? '7' : ''
    return [key[0], key[_random(6, 1)], key[_random(6, 1)], key[4] + seventh]
  }
}

function getStructure() {
  const songStructures = ['AAA', 'ABAB', 'ABABCB', 'ABABCAB']
  return songStructures[_random(songStructures.length - 1)]
}

/**
 * Return progression for each part of the structure
 * @param {string} structure
 * @param {[string]} key
 * @param {boolean} major
 */
function getProgressions(structure, key, major) {
  const parts = new Set(structure.split(''))
  const getProgression = major ? getMajorProgression : getMinorProgression
  return [...parts].map(() => getProgression(key))
}

/**
 *
 * @param {object} params anything passed into params will be kept
 * @param {number} params.bpm
 * @param {string} params.key
 * @param {boolean} params.major
 * @param {string} params.structure
 * @param {[string]} params.progressions
 */
function generateSong(params = {}) {
  let { bpm, key, keyRoot, major, structure, progressions } = params

  bpm = bpm || getBPM()
  keyRoot = keyRoot || getKeyRoot()
  key = key || getKey(keyRoot)
  major = major !== undefined ? major : Math.random() > 0.5
  structure = structure || getStructure()
  progressions = progressions || getProgressions(structure, key, major)
  return { bpm, key, keyRoot, major, structure, progressions }
}

export default {
  getBPM,
  getKey,
  getKeyRoot,
  getMajorProgression,
  getMinorProgression,
  getProgressions,
  getStructure,
  generateSong,
}
